#Capden

| pl                             | de                                 | en                         | cat                         |
| :---                           | :---                               | :---                       | :---:                       | 
| interfejs                      | die Schnittstelle                  | interface                  | common
| administrator bazy danych      | der Datenbankbenutzer              | database administrator     | database                    |
| baza danych                    | die Datenbank                      | database                   | database                    |      
| design bazy danych             | der Datenbankaufbau                | database design            | database                    |                    
| kolumna                        | die Spalte                         | database column            | database                    |                    
| model bazy danych              | das Datenbankmodell                | database model             | database                    |                    
| programista bazy danych        | der Datenbankprogrammierer         | database programmer        | database                    |
| relacyjny model bazy danych    | das relationale Datenbankmodell    | relational database model  | database                    |
| wiersz                         | die Zeile                          | database row               | database                    |
| system zarzadzania baza danych | das Datenbank Management System    | database management system | database                    |
| tabela bazy danych             | die Datenbanktabelle               | database table             | database                    |
| użytkownik bazy danych         | der Datenbankadministrator         | database user              | database                    |
| zapytanie do bazy danych       | die Datenbankabfrage               | database query             | database                    |
| dziedziczenie                  | das Erbe/-                         | inheritance                | oop                         |
| interfejs                      | die Schnittstelle                  | interface                  | oop                         |
| klasa                          | die Klasse                         | class                      | oop                         |